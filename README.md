# PB Polo Hospitalar

Projeto Hipotético Polo Hospitalar São Domingos

## **1. Introdução**

O trabalho a seguir é o resultado de atividades realizadas na disciplina de Projetos em Redes de Computadores afim de ter uma aproximação da teoria acadêmica com o que acontece na prática profissional.

### **1.1. Objetivo**

O objetivo deste Teste de Performance, é um levantamento de requisitos técnicos para um ponto de partida de desenvolvimento do projetos de redes para o Hospital São Domingos, onde vão ser definidas as funcionalidades e o escopo do projeto. O importante é compreender a clareza com o entendimento que o cliente repassa as informações.

### **1.2. Apresentação – FDTech**

![image](/uploads/81282537b29ed1cf42feb6c9d4a1978a/image.png)

A FDTec é uma empresa que vem se destacando em soluções de tecnologia para setores diversificados.
Localizado na cidade de Pedreiras – MA, atende clientes de todo estado do Maranhão e outros estados do Brasil.
Oferecemos diversos serviços e produtos para instalação e manutenção na área de Redes de Computadores, Informática, Data Centers.

**Tipos de Serviços:**
- Cabeamento Estruturado;
- Soluções em Fibra Óptica;
- Instalações Elétricas;
- Consultoria e Projetos de Redes;

## **2. Polo Hospitalar**

### **2.1. Apresentação do Cliente – Hospital São Domingos**

Este projeto visar atender o Hospital São Domingos, o mesmo fica localizado na cidade de São Luís no estado do Maranhão.
Sua unidade única fica no Endereço: Av. Jerônimo de Albuquerque, 540 - Bequimão, São Luís - MA, 65060-645.

![image](/uploads/fee9f60d0d25b3e570df207b7fca071a/image.png)

### **2.2. Identificando as necessidades do cliente**

O presente projeto visa propor a implementação da unidade Matriz para atendimento as novas unidades em construções e a implantação rede nas novas unidades a serem construídas.

**•	Implementação na Matriz**
Na unidade Matriz a finalidade é ampliar novos equipamentos para atendimento as novas unidades que estão sendo construídas.  
**•	Implantação nas unidades Sul e Leste**
Nestas unidades serão executados projetos lógicos, projetos físicos da rede, testes, otimização e documentação do projeto.

## **2.3. Localização geográfica das unidades do Hospital São Domingos**

A unidade da Matriz fica na região centro da capital de São Luís, para um melhor atendimento de seus clientes o Hospital está expandindo novas unidades na capital, sendo uma unidade na Zona SUL e uma unidade na Zona Leste, estas unidades suprirá clientes com vinda do interior do estado a fim de recursos de saúdes com mais complexidade, e também a população da grande capital maranhense.

![image](/uploads/6e511844c737cd8894487ac3d5d7f40c/image.png)

## **3. Projeto Físico da Rede**

### **3.1. Ambiente físico das instalações**

#### **Unidade Sul**

| Piso 1              | Piso 2               |
| :-----------------: | :------------------: |
| 04 Consultórios:    | 01 Recepção:         |
| 04 Computadores     | 02 Computadores      |
| 04 Impressoras      | 01 Impressoras       |
| 04 Aparelho Telefone|	02 Aparelho Telefone |
| 02 Computadores     |                      |
| 01 Impressoras      |                      |  
| 02 Aparelho Telefone|                      |
| 01 Administrativo:  |02 Consultórios:      |
| 01 Computadores     |02 Computadores       |
| 01 Impressoras      |02 Impressora         |
| 01 Aparelho Telefone|02 Aparelho Telefone  |
| 02 Computadores     |                      |
| 02 Impressoras      |                      |
| 02 Aparelho Telefone|                      |
| 01 Ambulatório:     |01 Tomografia:        |
| 02 Computadores     |02 Computadores       |
| 01 Impressoras      |01 Impressora         |
| 01 Aparelho Telefone|1 Aparelho Telefone	 |
| 02 Computadores     |                      |
| 01 Impressoras      |                      |
| 1 Aparelho Telefone |                      |
| 01 Recepção:        |01 Ultrassonografia:  |
| 02 Computadores     |02 Computadores       |
| 01 Impressoras      |01 Impressora         |
| 01 Aparelho Telefone|	1 Aparelho Telefone  |
| 02 Computadores     |                      |
| 01 Impressoras      |                      | 
| 01 Aparelho Telefone|                      |
| 01 Corredor:        |01 Raio X:            |
| 01 Aparelho Telefone|02 Computadores	     |
| 02 Computadores     |01 Impressora         |
| 01 Impressoras      |                      | 
| 01 CPD:             |01 Laboratório:       |
| 01 Computador       |02 Computadores       |
| 01 Impressora       |01 Impressora         |
| 01 Rack 32U         |01 Fisioterapia:      |
| Servidores          |01 Computadores       |
| Roteadores          |01 Impressora         |
| Switchs             |1 Aparelho Telefone   |
| 01 Aparelho Telefone|                      |

### **3.2. Diagrama de Redes Geral**

![image](/uploads/466de300a9db304086a96c11d8e9c064/image.png)

### **3.3. Link Ponto a Ponto (P2P)**

Entre as unidades será utilizado a contratação com link dedicado como solução de comunicação entre as Filiais, para a transmissão de dados, voz e vídeo. O link dedicado é um serviço especialmente desenvolvido para o setor corporativo, onde inúmeros benefícios são ofertados como maior escalabilidade, confiabilidade e segurança.

| Provedor  | Tecnologia  | Número Circuito  |  Suporte    | Banda   | Custo Mensal  |
| :-------: | :---------: | :-------------:  | :---------: | :-----: | :-----------: |
| Vivo      | IP MPLS     | 001.030.980      | 0800-235578 | 50Mbps  | R$ 4.000,00   |

### **3.4. Escopo de Fornecimento dos Equipamentos**

Vamos relacionar os principais equipamentos previstos para este projeto de fornecimento Cisco.

#### **3.4.1. Roteadores**

De forma a atender a comunicação de rede WAN da Matriz até as unidades filiais será utilizado os roteadores da família Cisco Catalyst 8500 Series Edge para a Matriz e roteadores de serviços integrados da família Cisco 4000 para as filiais.
Os roteadores da família Catalyst 8500 Series é uma solução em segurança de multicamadas e uma alternativa para tecnologia em nuvem.
O modelo Catalyst 8500L em sua performance de roteamento IPv4 e IPv6 de até 20 Gbps, capacidade de comutação padrão de 8,6 Gbps, Alta disponibilidade com redundância de alimentação e de software com IOS duplo, gestão de tafego com qualidade QoS.

![image](/uploads/a4ec745f23862b7ca681b2040ecd9453/image.png)
Vista frontal do Cisco Catalyst 1000 Series

Datasheet swt C1000-48P-4X-L:
https://www.cisco.com/c/en/us/products/collateral/switches/catalyst-1000-series-switches/nb-06-cat1k-ser-switch-ds-cte-en.html?oid=otren019232

Os roteadores da família ISR 4000 é uma solução inovadora para o segmento de filiais com os melhores serviços de rede.
O modelo ISR 4451 em sua performance de roteamento IPv4 e IPv6 de até 400 Mbps, capacidade de comutação padrão de 1,5 Gbps.

![image](/uploads/e5bdd4d9ee7756b12759114031c77841/image.png)
Vista frontal do ISR 4451

Datasheet ISR 4451:
https://www.cisco.com/c/en/us/products/collateral/routers/4000-series-integrated-services-routers-isr/data_sheet-c78-732542.html

#### **3.4.2. Switchs**

Está previsto o fornecimento do equipamento switches da família Cisco Catalyst 1000 Series para acesso à rede de comunicação LAN do Hospital São Domingos e suas Filiais Leste e Sul.
O equipamento de modelo C1000-48P-4X-L é composto por solução de comutação wire speed, com número fixo de portas Fast e Gigabit Ethernet, e possibilidade de empilhamento de até 8 unidades, Através das funcionalidades QoS, é possível manipular e priorizar pacotes de L7, como também controlar a banda disponibilizada para cada usuário.

![image](/uploads/39397c14429c4a7fca65bc838eae984e/image.png)
Vista frontal do Cisco Catalyst 1000 Series

Datasheet swt C1000-48P-4X-L:
https://www.cisco.com/c/en/us/products/collateral/switches/catalyst-1000-series-switches/nb-06-cat1k-ser-switch-ds-cte-en.html?oid=otren019232

#### **3.4.3. Access Point**

A comunicação por rede sem fio será atendida por dispositivos Acess Point da família Cisco Catalyst 9105AX Series.
O Acess Point é um produto 802.11ax de alta perfomance ou WI-FI 6, operam nas bandas de 2,4 GHz e 5 GHz, um de seus recurso é a captura inteligente, que investiga a rede e fornece ao Cisco DNA Center uma análise profunda da rede, em segurança o AP utiliza a tecnologia Cisco Trust Anchor onde fornece uma base altamente segura para os produtos Cisco.

![image](/uploads/91f19e6a3900891033e46873cbcb9e26/image.png)
Vista frontal do AP Cisco Catalyst 9105AX

Datasheet AP Catalyst 9105AX:
https://www.cisco.com/c/en/us/products/collateral/wireless/catalyst-9100ax-access-points/datasheet-c78-744062.html?oid=dstwls022713   

#### **3.4.3. Estimativa de Custos dos Equipamentos**

Com uma estimativa de custos sobre os equipamentos de fornecimento neste projeto, é possivel antecipar as necessidades de insumos e estimar os investimentos apropriados pelo Cliente.

![image](/uploads/de1470845cb26a9e787042ac600b78a3/image.png)

### **3.5. Pseudo Planta Baixa das Unidades Sul e Leste**

OBS: As informações deste tópico estão nas pastas de acesso inicial.

## **4. Cronograma**

O cronograma desse projeto está associado através de um diagrama aonde podemos visualizar os ciclos do projeto de um modo bruto, diagrama representado logo abaixo:
Segue link do cronograma realizada pela plataforma Monday.com
https://dorneles-team.monday.com/boards/1147243487/ 

![image](/uploads/1414fa8394ac93c0558a95fc46f00406/image.png)

## **5. Projeto Lógico de Redes**

Seguindo o caminho da nossa topologia física vamos definir como os nós da nossa rede se comunicam, O diagrama lógico tem por finalidade ser coeso e ilustrar as informações da comunicação do projeto de redes, evidencia como as configurações devem ser implementadas.

![image](/uploads/838c74ba4dbc2367202d03d4711d29e3/image.png)

### **5.1. Diagrama Lógico – Lan Matriz**

![image](/uploads/0e3b2b00e2521391f538a39984ad3bcb/image.png)

### **5.2. Diagrama Lógico – Lan Filial Sul**

![image](/uploads/4536e247b9c9907c2f52a0a6f23521ee/image.png)

### **5.3. Diagrama Lógico – Lan Filial Leste**

![image](/uploads/fb357d9f557ec6d34e81d8c59475de68/image.png)

### **5.4. Diagrama Lógico – Link Ponto a Ponto**

![image](/uploads/b9ed83a0c15489174339741d7c26340c/image.png)

### **5.5. Protocolo IPv4 e RFC 1918**

Visando um melhor suporte entre as unidades e por solicitação do nosso cliente, para este projeto vamos utilizar a versão IPv4 como protocolo de internet, de acordo com a norma RFC 1918 utilizaremos os endereços privados para o protocolo IPv4, onde alocamos para cada LAN a faixa de endereços privado 172.X.0.0. o "X" do 2º octeto representa a unidade alocada.

### **5.6. Endereçamento IP do Projeto**

Link com arquivo de todo endereçamento das 3 unidades deste projeto:
[Endereçamento_IP_PB_-_Gitlab.xlsx](/uploads/5516f075568cd737e55d6c380d732bde/Endereçamento_IP_PB_-_Gitlab.xlsx)

**•	Matriz:**
Para a Matriz, utilizamos um 172.18.0.0/24 e 172.19.0.0/24   para atender a necessidade dos 250 hosts disponibilizados pelo cliente, como o /24 atende cerca de 250 hosts temos uma margem tranquila já pensando em um planejamento futuro para mais escalabilidade da infraestrutura.

**•	Filial Sul:**
Para a Filial Sul, utilizamos um 172.20.0.0/24 e 172.21.0.0/24  em atendimento aos 100 hosts previsto em visita técnica, no entanto o prefixo /24 atende totalmente a infraestrutura local.

**•	Filial Leste:**	
Para a Filial Leste, utilizamos um 172.22.0.0/24 e 172.23.0.0/24  em atendimento as 50 hosts previsto em visita técnica, como o cliente já sinalizou possíveis ampliações, o prefixo /24 atende totalmente a infraestrutura local.

Todas as LANs terá endereços fixos e dinâmicos, para os endereços fixos será configurado manualmente, já os endereços dinâmicos utilizaremos o protocolo DHCP para automatizar a distribuição dos IPs.

### **5.6. VLSM**

Será utilizado VLSM para fragmentação das subredes visando configuração de Vlans, cada uma com o tamanho necessário para satisfazer os requisitos do projeto.


## **6. Modelo de Simulação**

### **6.1. Servidor DHCP**

Neste projeto vamos implementar em todas as filiais um servidor DHCP.
O protocolo DHCP é um serviço que responde cliente/servidor utilizado para automatizar as configurações do protocolo TCP/IP nos dispositivos da rede.

**Alguns benefícios do protocolo DHCP:**
- Automação no processo de distribuição dos Ips;
- Facilidade na alteração de configurações;
- Menos possibilidades de erros nas configurações;


Alguns parâmetros essenciais precisam ser configurados para o correto funcionamento do protocolo DHCP: Número IP; Máscara de sub-rede; Gateway, servidor DNS

Conforme imagem abaixo, servidor DHCP configurado das unidades:

**Unidade Matriz**

![image](/uploads/c505703543cfdaf8862092b3e21f7b00/image.png)

**Unidade Sul**

![image](/uploads/8df3e08ceee0606d2c5d5382c5f1c847/image.png)

**Unidade Leste**

![image](/uploads/1a39e29fe0adcb5f0328477a67c4c1c9/image.png)


